part of 'dishadd_bloc.dart';

@immutable
abstract class DishaddState {}

class DishaddInitial extends DishaddState {}

/* -------------------------------------------------------------------------- */
/*                     // !  Address STATE                                   */
/* -------------------------------------------------------------------------- */

class DishAddedLoadingState extends DishaddState {}

// !  Address LOADED GET REQUEST METHOD
class DishAddedState extends DishaddState {
  // List<Address> addressData;

  DishAddedState();

  @override
  List<Object> get props => [];
}

// ! END Address DATA
class DishAddedSuccessState extends DishaddState {}

class DishAddedErrorState extends DishaddState {
  final String error;
  DishAddedErrorState({required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AddressFailure(error: $error)';
}
