part of 'dishadd_bloc.dart';

@immutable
abstract class DishaddEvent {}

/* -------------------------------------------------------------------------- */
/*                          // ! Address SAVE BUTTON                          */
/* -------------------------------------------------------------------------- */

// ignore: must_be_immutable
class DishAddedBtnEvent extends DishaddEvent {
  String? dName;
  String? dDesc;
  String? price;
  String? disPer;
  String? disprice;
  String? cusi;
  String? diet;
  String? spicy;
  String? allerg;
  String? made;
  String? cate;
  String? pretime;
  String? hightl;
  dynamic img1;
  dynamic img2;
  dynamic img3;
  dynamic img4;

  DishAddedBtnEvent({
    this.dName,
    this.dDesc,
    this.price,
    this.disPer,
    this.disprice,
    this.cusi,
    this.diet,
    this.spicy,
    this.allerg,
    this.made,
    this.cate,
    this.pretime,
    this.hightl,
    this.img1,
    this.img2,
    this.img3,
    this.img4,
  });

  @override
  String toString() => 'FnameSaveBtn { fname: $dName }';
}
