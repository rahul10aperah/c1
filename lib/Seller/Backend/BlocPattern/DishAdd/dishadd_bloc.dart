import 'package:comp3/Seller/Backend/SellerRespsitory/SelReg/SelDishAddedResp.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'dishadd_event.dart';
part 'dishadd_state.dart';

// class DishaddBloc extends Bloc<DishaddEvent, DishaddState> {
//   DishaddBloc() : super(DishaddInitial()) {
//     on<DishaddEvent>((event, emit) {
//       // TODO: implement event handler
//     });
//   }
// }

class DishaddBloc extends Bloc<DishaddEvent, DishaddState> {
  //  orc Adding Repo for data logic

  SelDishAddedRespo addRespo = SelDishAddedRespo();

  DishaddBloc() : super(DishaddInitial());

  @override
  Stream<DishaddState> mapEventToState(
    DishaddEvent event,
  ) async* {
    /* -------------------------------------------------------------------------- */
    /*                  // ! Address ADDED , ADDING  , DELETING BLOC                 */
    /* -------------------------------------------------------------------------- */
    if (event is DishAddedBtnEvent) {
      List<dynamic> regDat = await addRespo.selDishAddedR(
        dName: event.dName,
        dDesc: event.dDesc,
        price: event.price,
        disPer: event.disPer,
        disprice: event.disprice,
        cusi: event.cusi,
        diet: event.diet,
        spicy: event.spicy,
        allerg: event.allerg,
        made: event.made,
        cate: event.cate,
        pretime: event.pretime,
        hightl: event.hightl,
        img1: event.img1,
        img2: event.img2,
        img3: event.img3,
        img4: event.img4,
      );

      // print('ADDRESS DATA :- $addrList');
      yield DishAddedSuccessState();
    }
  }
}
