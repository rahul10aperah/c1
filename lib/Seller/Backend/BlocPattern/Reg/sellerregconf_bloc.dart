import 'dart:io';

import 'package:comp3/Seller/Backend/SellerModel/regModel.dart';
import 'package:comp3/Seller/Backend/SellerRespsitory/SelReg/SelRegResp.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'sellerregconf_event.dart';
part 'sellerregconf_state.dart';

class SellerregconfBloc extends Bloc<SellerregconfEvent, SellerregconfState> {
  //  orc Adding Repo for data logic

  SelRegDataRespo addRespo = SelRegDataRespo();

  SellerregconfBloc() : super(SellerregconfInitial());

  @override
  Stream<SellerregconfState> mapEventToState(
    SellerregconfEvent event,
  ) async* {
    /* -------------------------------------------------------------------------- */
    /*                  // ! Address ADDED , ADDING  , DELETING BLOC                 */
    /* -------------------------------------------------------------------------- */
    if (event is SelRegSaveBtnEvent) {
      // dynamic regM = await addRespo.selRegModel(regM: event.regModel);
      bool regData = await addRespo.selRegData(
          fname: event.fname,
          lname: event.lname,
          phone: event.phone,
          dateC: event.dateC,
          street: event.street,
          city: event.city,
          state: event.state,
          zipCode: event.zipCode,
          country: event.country,
          fp: event.fp,
          fodper: event.fodper,
          govId: event.govId,
          special: event.special,
          dayC: event.dayC,
          time1: event.time1,
          time2: event.time2,
          exp: event.exp,
          hear: event.hear,
          interset: event.interset,
          delMode: event.delMode,
          refferal: event.refferal,
          otpD: event.OtpD);

      yield SellerRegSuccessState();
    }

    // ! otp send Event
    if (event is SelRegOtpBtnEvent) {
      bool sedMOtp = await addRespo.sedOtp(phone: event.phone);

      yield SelOtpSendSuccessState();
    }

    // ! RESEND  otp Event
    if (event is SelRegReOtpBtnEvent) {
      bool resedMOtp = await addRespo.resedOtp(phone: event.phone);

      yield SelOtpResendSuccessState();
    }
  }
}
