part of 'sellerregconf_bloc.dart';

@immutable
abstract class SellerregconfState {}

class SellerregconfInitial extends SellerregconfState {}

/* -------------------------------------------------------------------------- */
/*                     // !  ADD  STATE                                   */
/* -------------------------------------------------------------------------- */

class SellerRegLoadingState extends SellerregconfState {}

// !  Address LOADED GET REQUEST METHOD
class SellerRegState extends SellerregconfState {
  // List<Address> addressData;

  SellerRegState();

  @override
  List<Object> get props => [];
}

// ! END Address DATA
class SellerRegSuccessState extends SellerregconfState {}

class SellerRegErrorState extends SellerregconfState {
  final String error;
  SellerRegErrorState({required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AddressFailure(error: $error)';
}

// ! otp send State
class SelOtpSendSuccessState extends SellerregconfState {}

class SelOtpResendSuccessState extends SellerregconfState {}
