part of 'sellerregconf_bloc.dart';

@immutable
abstract class SellerregconfEvent {}

// !  Address FETCH Event
class FetchSelRegDataEvent extends SellerregconfEvent {
  @override
  List<Object> get props => [];
}

/* -------------------------------------------------------------------------- */
/*                          // ! Address SAVE BUTTON                          */
/* -------------------------------------------------------------------------- */

// ignore: must_be_immutable
class SelRegSaveBtnEvent extends SellerregconfEvent {
  // SellerRegModel regModel;
  // SelRegSaveBtnEvent({required this.regModel});
  String? fname;
  String? lname;
  String? phone;
  String? dateC;
  String? street;
  String? city;
  String? state;
  String? zipCode;
  String? country;
  String? fp;
  // File? fodper;
  // File? govId;
  dynamic fodper;
  dynamic govId;
  String? dayC;
  String? time1;
  String? time2;
  String? exp;
  String? refferal;
  String? hear;
  String? interset;
  String? special;
  String? delMode;
  String? OtpD;

  SelRegSaveBtnEvent(
      {this.fname,
      this.lname,
      this.phone,
      this.country,
      this.city,
      this.dateC,
      this.dayC,
      this.exp,
      this.fp,
      this.fodper,
      this.govId,
      this.hear,
      this.interset,
      this.refferal,
      this.special,
      this.state,
      this.street,
      this.time1,
      this.time2,
      this.zipCode,
      this.delMode,
      this.OtpD});

  @override
  String toString() => 'FnameSaveBtn { fname:  }';
}

// ! OTP SEND Event
class SelRegOtpBtnEvent extends SellerregconfEvent {
  String? phone;

  SelRegOtpBtnEvent({this.phone});

  @override
  String toString() => 'FnameSaveBtn { phone: $phone }';
}

// !  RESEND OTP  Event
class SelRegReOtpBtnEvent extends SellerregconfEvent {
  String? phone;

  SelRegReOtpBtnEvent({this.phone});

  @override
  String toString() => 'FnameSaveBtn { phone: $phone }';
}
