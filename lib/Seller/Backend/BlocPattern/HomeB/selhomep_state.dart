part of 'selhomep_bloc.dart';

@immutable
abstract class SelhomepState {}

class SelhomepInitial extends SelhomepState {}

/* -------------------------------------------------------------------------- */
/*                     // !  ADD  STATE                                   */
/* -------------------------------------------------------------------------- */

class SelHomeLoadingState extends SelhomepState {}

// !  Address LOADED GET REQUEST METHOD
class SelHomeState extends SelhomepState {
  // List<Address> addressData;

  SelHomeState();

  @override
  List<Object> get props => [];
}

// ! END Address DATA
class SelHomeSuccessState extends SelhomepState {}

class SelHomeErrorState extends SelhomepState {
  final String error;
  SelHomeErrorState({required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AddressFailure(error: $error)';
}
