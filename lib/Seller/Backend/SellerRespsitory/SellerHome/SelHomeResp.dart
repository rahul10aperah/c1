// import 'dart:convert';
// import 'package:comp2/Model/category.dart';
// import 'package:comp2/Model/user.dart';
// import 'package:comp2/util/app_constants.dart';
// import 'package:comp2/util/shared_helper.dart';
// // import 'package:flutter/cupertino.dart';
// import 'package:http/http.dart' as http;

// /* -------------------------------------------------------------------------- */
// /*           
//               !     SHOW ALL PRODUCT LIST WITH TWO PATTERN :-  
//               ! 1. MAKEING ANOTHER ABSTRACT CLASS :- LIKE THIS PAGE 
//               ! 2. WITHOUT ANY ABSTRACT CLASS :- REGISTER, LOGIN PAGE   
//                                                                                 */
// /* -------------------------------------------------------------------------- */
// ValueNotifier<User> currentUser = ValueNotifier(User());

// class ProductDataRespo {
//   @override
//   Future<List<Category>> getProduct() async {
//     // ! TAKING TOKEN DATA
//     SharedHelper shared = SharedHelper();
//     var toekn = json.decode(await shared.getString('current_user'));

//     print('tokn ${toekn}');
//     final client = http.Client();

//     final response = await client.get(
//       Uri.parse(
//         URLConstants.userHome,
//       ),
//       headers: {
//         'Authorization': 'Bearer ' + toekn['token'],
//         'Content-Type': 'application/json'
//       },
//     );

//     print('Status Code ' + response.statusCode.toString());
//     print('Response ' + response.body);

//     if (response.statusCode == 200 ||
//         response.statusCode == 201 ||
//         response.statusCode == 501 ||
//         response.statusCode == 404 ||
//         response.statusCode == 403 ||
//         response.statusCode == 400 ||
//         response.statusCode == 409 ||
//         response.statusCode == 500) {
//       var data = jsonDecode(response.body);
//       print('Data   $data');
//       print(response.statusCode);
//       return data;
//     } else {
//       throw Exception(response.body);
//     }
//   }
// }
