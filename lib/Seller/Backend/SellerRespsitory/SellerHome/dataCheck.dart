// import 'dart:convert';

// import 'package:comp2/Model/category.dart';
// import 'package:comp2/Model/user.dart';
// import 'package:comp2/Seller/Backend/SellerRespsitory/SellerHome/SelHomeResp.dart';
// import 'package:comp2/util/app_constants.dart';
// import 'package:comp2/util/shared_helper.dart';
// import 'package:flutter/material.dart';

// import 'package:http/http.dart' as http;

// // ValueNotifier<User> currentUser = ValueNotifier(User());

// // ! Data Check show
// class SellerDataCheck extends StatelessWidget {
//   const SellerDataCheck({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Padding(
//       padding: const EdgeInsets.only(top: 20.0),
//       child: Container(
//           child: Column(
//         children: [Text('This Data'), ProdData()],
//       )),
//     ));
//   }
// }

// // ! PRODUCT Deails
// // ignore: must_be_immutable
// class ProdData extends StatelessWidget {
//   ProdData({Key? key}) : super(key: key);

//   ProductDataRespo prodData = ProductDataRespo();

//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: prodData.getProduct(),
//       builder: (context, AsyncSnapshot<List?> snapshot) {
//         if (snapshot.hasData) {
//           return Text(snapshot.data.toString());
//           // GridView.builder(
//           //   shrinkWrap: true,
//           //   physics: ScrollPhysics(),
//           //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//           //     crossAxisCount: 2,
//           //     childAspectRatio: 0.80,
//           //   ),
//           //   itemCount: snapshot.data!.length,
//           //   itemBuilder: (
//           //     context,
//           //     int index,
//           //   ) {
//           //     return ProdGridListShow(prodNumber: snapshot.data![index]);
//           //   },
//           // );
//         }
//         return Center(
//           child: CircularProgressIndicator(),
//         );
//       },
//     );
//   }
// }

// // ! Prouct data show
// class ProdGridListShow extends StatelessWidget {
//   final dynamic prodNumber;

//   ProdGridListShow({
//     Key? key,
//     this.prodNumber,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(10),
//       child: SizedBox(
//         width: 160.0,
//         child: Wrap(
//           children: <Widget>[
//             InkWell(
//               child: Card(
//                 child: Column(
//                   children: [
//                     // ! PRODUCT PI
//                     Padding(
//                       padding: const EdgeInsets.all(10),
//                       child: Container(
//                         width: double.infinity,
//                         height: 110,
//                         child: Image.network(
//                           prodNumber['file'] == null ? '' : prodNumber['file'],
//                           fit: BoxFit.cover,
//                         ),
//                       ),
//                     ),
//                     // ! END PRODUCT PIC
//                     ListTile(
//                       title: Text(
//                         prodNumber['name'],
//                         maxLines: 1,
//                         style: TextStyle(
//                             fontSize: 15, fontWeight: FontWeight.bold),
//                       ),
//                       subtitle: Text(
//                         prodNumber['description'],
//                         maxLines: 1,
//                         style: TextStyle(fontSize: 12.0),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
