import 'dart:convert';
import 'dart:io';
import 'package:comp3/util/app_constants.dart';
import 'package:comp3/util/shared_helper.dart';
import 'package:http/http.dart' as http;

// ValueNotifier<User> currentUser = ValueNotifier(User());

class SelDishAddedRespo {
  Future selDishAddedR({
    String? dName,
    String? dDesc,
    String? price,
    String? disPer,
    String? disprice,
    String? cusi,
    String? diet,
    String? spicy,
    String? allerg,
    String? made,
    String? cate,
    String? pretime,
    String? hightl,
    dynamic img1,
    dynamic img2,
    dynamic img3,
    dynamic img4,
  }) async {
    print('dishname $dName');

    // ! TAKING TOKEN DATA
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));
    // ! Header fILE
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      'Content-Type': 'multipart/form-data',
    };

    Map<String, String> bodyD = {
      "dishName": dName.toString(),
      "description": dDesc.toString(),
      "price": price != null ? price : '',
      "discountPercentage": disPer != null ? disPer : '',
      "discountPrice": disprice != null ? disprice : '',
      "cuisine": cusi != null ? cusi : '',
      "dietary": diet != null ? diet : '',
      "spicy": spicy != null ? spicy : '',
      "allergen": allerg != null ? allerg : '',
      "marking": made != null ? made : '',
      "category": cate != null ? cate : '',
      "preperationTime": pretime != null ? pretime : '',
      "otherHighlight": hightl != null ? hightl : '',
    };

    List fileDat = [
      img1,
      img2,
      img3,
      img4,
    ];

    var reques =
        http.MultipartRequest('POST', Uri.parse(URLConstants.selDishAdd));

    // ! imge in loop
    List<http.MultipartFile> newList = [];

    for (var img in fileDat) {
      if (img != "") {
        var multipartFile = await http.MultipartFile.fromPath(
          'Photos',
          File(img).path,
          filename: img.split('/').last,
        );
        newList.add(multipartFile);
      }
    }
    reques.files.addAll(newList);

// ! for Loop
    reques.headers.addAll(headers);
    reques.fields.addAll(bodyD);

    // send
    var response = await reques.send();

    print(response.statusCode);
  }
}
