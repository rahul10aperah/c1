import 'dart:convert';
import 'dart:io';
import 'package:comp3/Seller/Backend/SellerModel/regModel.dart';
import 'package:comp3/util/app_constants.dart';
import 'package:comp3/util/shared_helper.dart';
import 'package:http/http.dart' as http;

// ValueNotifier<User> currentUser = ValueNotifier(User());

class SelRegDataRespo {
  // ! SEND OTP
  Future<bool> sedOtp({String? phone}) async {
    final client = http.Client();
    final res = await client.post(
      Uri.parse(URLConstants.sendOtpMbl),
      body: json.encode({"phone": phone}),
    );
    print(res.body);
    print(res.statusCode);
    if (res.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  // ! RESEND OTP
  Future<bool> resedOtp({String? phone}) async {
    final client = http.Client();
    final res = await client.post(
      Uri.parse(URLConstants.sendOtpMbl),
      body: json.encode({"phone": phone}),
    );
    print(res.body);
    print(res.statusCode);
    if (res.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

// // ! SELLER REGISTER
  Future<bool> selRegData({
    String? fname,
    String? lname,
    String? phone,
    String? dateC,
    String? street,
    String? city,
    String? state,
    String? zipCode,
    String? country,
    String? fp,
    // File? fodper,
    // File? govId,
    dynamic fodper,
    dynamic govId,
    String? dayC,
    String? time1,
    String? time2,
    String? exp,
    String? refferal,
    String? hear,
    String? interset,
    String? special,
    String? delMode,
    String? otpD,
  }) async {
    print(
        '----------------------------------------------------------------------');
    print('this is name $fname');
    print('this is lname$lname');
    print('this is phone $phone');
    print('this date $dateC');
    print('this is street $street');
    print('this is city $city');
    print('this is zip $zipCode');
    print('this is state $state');
    print('this is coun $country');
    print('this is day $dayC');
    print('this is time1 $time1');
    print('this is time2 $time2');
    print('this exp $exp');
    print('this is har $hear');
    print('this Ref $refferal');
    print('this Spec $special');
    print('this is dom  $delMode');
    print('this is In $interset');
    print('this is food permit $fp');
    print('this is FoodPermit ${fodper}');
    print('this is GovId ${govId}');
    print(otpD);

    // ! TAKING TOKEN DATA
    SharedHelper shared = SharedHelper();
    var tokn = json.decode(await shared.getString('current_user'));

    // ! Header fILE
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + tokn['token'],
      // 'Content-Type': 'multipart/form-data',
    };

    // print('this is Token ${tokn['token']}');
    Map<String, String> bodyD = {
      "firstName": '$fname',
      "lastName": '$lname',
      "timeToContact": '$dateC',
      "phone": '$phone',
      "street": '$street',
      "city": '$city',
      "state": '$state',
      "zipCode": '$zipCode',
      "country": '$country',
      "foodPermit": '$fp',
      "cuisineSpecialize": '$special',
      "cookDay": '$dateC',
      "cookTimeFrom": '$time1',
      "cookTimeTo": '$time2',
      "modeOfDelivery": '$delMode',
      "experience": '$exp',
      "hearAboutUs": '$hear',
      "interests": '$interset',
      "sellerReference": '$refferal',
      "phoneVerify": '$otpD',
      "foodPermitImage": fodper.toString(),
      //  await http.MultipartFile.fromPath(
      //     'Photos',
      //     fodper,
      //     filename: img.split('/').last,
      //   ),
      "govtIdImage": govId.toString(),
    };

    // List fileDat = [
    //   fodper,
    //   govId,
    // ];
    // print('File dATE ${fileDat}');
    var reques =
        http.MultipartRequest('POST', Uri.parse(URLConstants.selDishAdd));

    // ! imge in loop

    var pic1 = await http.MultipartFile.fromPath(
      'foodPermitImage',
      fodper,
    );

    var pic2 = await http.MultipartFile.fromPath(
      'govtIdImage',
      govId,
    );
    if (fodper != null) {
      reques.files.add(pic1);
    }

    if (govId != null) {
      reques.files.add(pic2);
    }

    reques.headers.addAll(headers);
    reques.fields.addAll(bodyD);

    // send
    var response = await reques.send();
    print('THis is response ${response}');
    print(response.statusCode);

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  // Future<bool> selRegModel({required SellerRegModel regM}) async {
  //   // ! TAKING TOKEN DATA
  //   SharedHelper shared = SharedHelper();
  //   var tokn = json.decode(await shared.getString('current_user'));
  //   // ! Header fILE
  //   Map<String, String> headers = {
  //     'Authorization': 'Bearer ' + tokn['token'],
  //     'Content-Type': 'multipart/form-data',
  //   };

  //   print('---------------------------------------------------------');
  //   print('this is model data ${regM.fname}');
  //   var reques =
  //       http.MultipartRequest('POST', Uri.parse(URLConstants.selDishAdd));
  //   reques.headers.addAll(headers);
  //   reques.fields.addAll({
  //     "firstName": regM.fname!,
  //     "lastName": regM.lname!,
  //     "timeToContact": regM.dateC!,
  //     "phone": regM.phone!,
  //     "street": regM.street!,
  //     "city": regM.city!,
  //     "state": regM.state!,
  //     "zipCode": regM.zipCode!,
  //     "country": regM.country!,
  //     "foodPermit": regM.fp!,
  //     "cuisineSpecialize": regM.special!,
  //     "cookDay": regM.dateC!,
  //     "cookTimeFrom": regM.time1!,
  //     "cookTimeTo": regM.time2!,
  //     "modeOfDelivery": regM.delMode!,
  //     "experience": regM.exp!,
  //     "hearAboutUs": regM.hear!,
  //     "interests": regM.interset!,
  //     "sellerReference": regM.refferal!,
  //     "phoneVerify": regM.otpD!,
  //     "foodPermitImage": regM.fodper,
  //     "govtIdImage": regM.govId,
  //   });

  //   var res = await reques.send();

  //   print(res);
  //   print(res.statusCode);
  //   if (res.statusCode == 200) {
  //     return true;
  //   }
  //   return false;
  // }
}
