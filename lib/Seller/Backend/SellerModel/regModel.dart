import 'dart:convert';

SellerRegModel sellerRegModelFromMap(String str) =>
    SellerRegModel.fromMap(json.decode(str));

String sellerRegModelToMap(SellerRegModel data) => json.encode(data.toMap());

class SellerRegModel {
  SellerRegModel(
      {this.fname,
      this.lname,
      this.phone,
      this.country,
      this.city,
      this.dateC,
      this.dayC,
      this.exp,
      this.fp,
      this.fodper,
      this.govId,
      this.hear,
      this.interset,
      this.refferal,
      this.special,
      this.state,
      this.street,
      this.time1,
      this.time2,
      this.zipCode,
      this.delMode,
      this.otpD});

  String? fname;
  String? lname;
  String? phone;
  String? dateC;
  String? street;
  String? city;
  String? state;
  String? zipCode;
  String? country;
  String? fp;
  // File? fodper;
  // File? govId;
  dynamic fodper;
  dynamic govId;
  String? dayC;
  String? time1;
  String? time2;
  String? exp;
  String? refferal;
  String? hear;
  String? interset;
  String? special;
  String? delMode;
  String? otpD;

  factory SellerRegModel.fromMap(Map<String, dynamic> json) => SellerRegModel(
        fname: json["fname"],
        lname: json["lname"],
        phone: json["phone"],
        dateC: json["dateC"],
        street: json["street"],
        city: json["city"],
        state: json["state"],
        zipCode: json["zipCode"],
        country: json["country"],
        fp: json["fp"],
        fodper: json["fodper"],
        govId: json["govId"],
        dayC: json["dayC"],
        time1: json["time1"],
        time2: json["time2"],
        exp: json["exp"],
        refferal: json["refferal"],
        special: json["special"],
        delMode: json["delMode"],
        otpD: json["otpD"],
      );

  Map<String, dynamic> toMap() => {
        "fname": fname,
        "lname": lname,
        "phone": phone,
        "dateC": dateC,
        "street": street,
        "city": city,
        "state": state,
        "zipCode": zipCode,
        "country": country,
        "fp": fp,
        "fodper": fodper,
        "govId": govId,
        "dayC": dayC,
        "time1": time1,
        "time2": time2,
        "exp": exp,
        "refferal": refferal,
        "special": special,
        "delMode": delMode,
        "otpD": otpD,
      };

  // Map toJson() {
  //   var map = Map<String, dynamic>();
  //   map["fname"] = fname;
  //   map["lname"] = lname;
  //   map["phone"] = phone;
  //   map["dateC"] = dateC;
  //   map["street"] = street;
  //   map["city"] = city;
  //   map["state"] = state;
  //   map["zipCode"] = zipCode;
  //   map["country"] = country;
  //   map["fp"] = fp;
  //   map["fodper"] = fodper;
  //   map["govId"] = govId;
  //   map["dayC"] = dayC;
  //   map["time1"] = time1;
  //   map["time2"] = time2;
  //   map["exp"] = exp;
  //   map["refferal"] = refferal;
  //   map["special"] = special;
  //   map["delMode"] = delMode;
  //   map["otpD"] = otpD;
  //   return map;
  // }
}
