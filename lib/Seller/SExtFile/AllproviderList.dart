import 'package:comp3/Seller/Backend/BlocPattern/DishAdd/dishadd_bloc.dart';
import 'package:comp3/Seller/Backend/BlocPattern/Reg/sellerregconf_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/single_child_widget.dart';

class MainBloc {
  static List<SingleChildWidget> allBlocs() {
    return [
      // ChangeNotifierProvider(create: (ctx) => AllFormValdation()),
      BlocProvider(create: (ctx) => SellerregconfBloc()),
      BlocProvider(create: (ctx) => DishaddBloc()),
    ];
  }
}
