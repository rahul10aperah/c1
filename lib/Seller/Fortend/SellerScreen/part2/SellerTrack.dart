import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/Menu/menu.dart';
import 'package:flutter/material.dart';

import 'SellerRevRepoer.dart';

class SellerTrackOrderScr extends StatefulWidget {
  static const routeName = '/SellerTrackOrderScr';
  const SellerTrackOrderScr({Key? key}) : super(key: key);

  @override
  _SellerTrackOrderScrState createState() => _SellerTrackOrderScrState();
}

class _SellerTrackOrderScrState extends State<SellerTrackOrderScr> {
  // ! box item
  dynamic selectedIndex = 'Pending';
  void onItemTapped(String? index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Track Order'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // ! Menu Header
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: double.infinity,
                    child: Row(
                      children: [
                        SellerMenuTxt('Pending',
                            selectedIndex: selectedIndex,
                            onItemTapped: onItemTapped),
                        SellerMenuTxt('Accepted',
                            selectedIndex: selectedIndex,
                            onItemTapped: onItemTapped),
                        SellerMenuTxt('Completed',
                            selectedIndex: selectedIndex,
                            onItemTapped: onItemTapped),
                        SellerMenuTxt('Rejected',
                            selectedIndex: selectedIndex,
                            onItemTapped: onItemTapped)
                      ],
                    ),
                  ),
                ),
                // ! Card Item
                Card(
                  child: Column(
                    children: [
                      SellerTableH5(
                        t1: 'SL.',
                        t2: 'Date & Time',
                        t3: 'Order #',
                        t4: 'Order By',
                        t5: 'Collection Type',
                        fSize: 14,
                      ),
                      TableI1()
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// ! table Item
class TableI1 extends StatelessWidget {
  TableI1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10),
      child: SizedBox(
        height: 700,
        width: 500,
        child: ListView.builder(
            itemCount: 3,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  // ! Cricle Border

                  borderRadius: BorderRadius.circular(45),
                ),
                child: Card(
                  child: Column(
                    children: [
                      Table(
                          border:
                              TableBorder.all(width: .5, color: Colors.black),
                          children: [
                            TableRow(children: [
                              tableH('1', fWeig: FontWeight.bold),
                              tableH('date 10/5/21', fWeig: FontWeight.bold),
                              tableH('id 13258', fWeig: FontWeight.bold),
                              tableH('rahul', fWeig: FontWeight.bold),
                              tableH('pickup 4km', fWeig: FontWeight.bold),
                            ])
                          ]),
                      Table(children: [
                        TableRow(children: [
                          tableH('Item', fWeig: FontWeight.bold),
                          tableH('Qty', fWeig: FontWeight.bold),
                          tableH('TotalAmmount', fWeig: FontWeight.bold),
                        ]),
                        TableRow(children: [
                          tableH('p1'),
                          tableH('5'),
                          tableH('1000'),
                        ]),
                        TableRow(children: [
                          tableH('p2'),
                          tableH('10'),
                          tableH('2000'),
                        ])
                      ]),
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }

  // ! TABLE Header REPORT
  Widget tableH(String t, {FontWeight? fWeig}) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Text(t,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 14, fontWeight: fWeig)),
    );
  }
}
