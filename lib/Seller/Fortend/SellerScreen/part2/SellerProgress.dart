import 'package:flutter/material.dart';

class SellerProgressScr extends StatelessWidget {
  static const routeName = '/SellerProgressScr';
  const SellerProgressScr({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Progress'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SellerCardItem(
                    title: 'Earnings',
                    price1: 800.20,
                    price2: 800.20,
                    time1: 'Febuary',
                    time2: 'AllTime',
                    linkp: 'See more'),
                SellerCardItem(
                    title: 'Earnings',
                    price1: 800.20,
                    price2: 800.20,
                    time1: 'Febuary',
                    time2: 'AllTime',
                    linkp: 'See more'),
                SellerCardItem(
                    title: 'Earnings',
                    price1: 800.20,
                    price2: 800.20,
                    time1: 'Febuary',
                    time2: 'AllTime',
                    linkp: 'See more'),
                SellerCardItem(
                    title: 'Earnings',
                    price1: 800.20,
                    price2: 800.20,
                    time1: 'Febuary',
                    time2: 'AllTime',
                    linkp: 'See more')
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// ! CARD ITEM FOR ITEM
class SellerCardItem extends StatelessWidget {
  final dynamic title;
  final dynamic price1;
  final dynamic price2;
  final dynamic time1;
  final dynamic time2;
  final dynamic linkp;

  SellerCardItem(
      {Key? key,
      this.title,
      this.price1,
      this.price2,
      this.time1,
      this.time2,
      this.linkp})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
          child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            allTxt(title.toString(), siz: 18, col: Colors.red),
            // ! ROW FOR PRICE
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  allTxt("${price1.toString()}", siz: 15, col: Colors.black),
                  allTxt("${price2.toString()}", siz: 15, col: Colors.black),
                ],
              ),
            ),
            // ! ROW FOR TIME
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  allTxt(time1.toString(), siz: 13, col: Colors.brown),
                  allTxt(time2.toString(), siz: 13, col: Colors.brown)
                ],
              ),
            ),
            // ! LINK headLine
            allTxt(
              linkp != null ? linkp : '',
              siz: 13,
              txtd: TextDecoration.underline,
              col: Colors.blueAccent,
            )
          ],
        ),
      )),
    );
  }

  // ! all Text
  Widget allTxt(String p, {double? siz, Color? col, TextDecoration? txtd}) {
    return Text(p,
        style: TextStyle(
            fontSize: siz,
            fontWeight: FontWeight.bold,
            color: col,
            decoration: txtd));
  }
}
