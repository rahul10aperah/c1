import 'package:flutter/material.dart';

class SellerReviewScr extends StatelessWidget {
  static const routeName = '/SellerReviewScr';
  const SellerReviewScr({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Review'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SellerReviewHeader(rat1: 4.3, rat2: 4.5, mon: 'March'),
                // ! DIVIDER WITH tEXT
                Row(children: <Widget>[
                  Expanded(child: Divider()),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0, right: 5),
                    child: Text(
                      "15 Reviews",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                  ),
                  Expanded(child: Divider()),
                ]),
                // ! review item
                SellerReviewItem(
                  t3: 'Earnings',
                  t1: 'Febuary',
                  t2: 'AllTime',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// ! Review Header
class SellerReviewHeader extends StatelessWidget {
  final dynamic rat1;
  final dynamic rat2;
  final dynamic mon;
  SellerReviewHeader({Key? key, this.rat1, this.rat2, this.mon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [SellerStarIcon(), line1Txt(rat1.toString())],
                  ),
                  Text('Month'),

                  // ! all time
                  Row(
                    children: [
                      SellerStarIcon(),
                      line1Txt(rat2.toString()),
                    ],
                  )
                ],
              ),

              // ! NEXTLINE
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  line2Txt(mon.toString()),
                  // ! all time
                  Row(
                    children: [line2Txt(mon.toString()), btn()],
                  ),

                  line2Txt('AllTime'),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget btn() {
    return Padding(
      padding: const EdgeInsets.only(left: 3.0, bottom: 5),
      child: Container(
        constraints: BoxConstraints(
            maxWidth: 35.0,
            minHeight: 25.0,
            maxHeight: 25.0), // min sizes for Material buttons
        alignment: Alignment.center,
        color: Colors.blue,
        child: InkWell(
          child: Text(
            'view',
            style: TextStyle(fontSize: 12, color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget line1Txt(String t) {
    return Text(
      t,
      style: TextStyle(
          fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
    );
  }

  Widget line2Txt(String t) {
    return Text(
      t,
      style: TextStyle(fontSize: 15),
    );
  }
}

// ! Review Item
class SellerReviewItem extends StatelessWidget {
  final dynamic t1;
  final dynamic t2;
  final dynamic t3;
  SellerReviewItem({
    Key? key,
    this.t1,
    this.t2,
    this.t3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // ! Star PAGE
              starc(),

              txtM(
                t1,
                fSize: 17,
                col: Colors.black38,
                fWeg: FontWeight.bold,
              ),
              txtM(t2, fSize: 15, col: Colors.black, fWeg: FontWeight.bold),
              txtM(
                t3,
                fSize: 15,
                col: Colors.red,
                fWeg: FontWeight.bold,
              ),
            ],
          ),
        ),
      ),
    );
  }

  // ! STAR COUNT
  Widget starc() {
    return SizedBox(
      height: 40,
      width: 600,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 4,
          itemBuilder: (context, index) {
            return SellerStarIcon();
          }),
    );
  }

  Padding txtM(dynamic t, {double? fSize, Color? col, FontWeight? fWeg}) {
    return Padding(
      padding: const EdgeInsets.only(top: 3.0, left: 10, bottom: 2),
      child: Text(t,
          style: TextStyle(
            fontSize: fSize,
            color: col,
            fontWeight: fWeg,
          )),
    );
  }
}

// ! star icon
class SellerStarIcon extends StatelessWidget {
  const SellerStarIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Icon(Icons.star, color: Colors.yellow);
  }
}
