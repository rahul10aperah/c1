import 'dart:io';
import 'package:comp3/Seller/Backend/BlocPattern/Reg/sellerregconf_bloc.dart';
import 'package:comp3/Seller/Backend/SellerModel/regModel.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/AlertDi/AlertD.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/SellerBtn.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/SellerConTxt.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/SellerFormField.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/Selleruplod.dart';
import 'package:comp3/util/common.dart';
import 'package:comp3/util/input_validation.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';
import 'package:path/path.dart';

import '/util/style.dart';
import 'SellerHome.dart';

class SellerRegisterScr extends StatelessWidget {
  static const routeName = '/sellersignupscreens';
  SellerRegisterScr({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Seller'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //  ! REGISTER PAGE
                // SellerSignUpForm()
                SellerRegisterOne()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                          // ! REGISTER PAGE LOGIC                          */
/* -------------------------------------------------------------------------- */
class SellerRegisterOne extends StatelessWidget {
  SellerRegisterOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SellerregconfBloc, SellerregconfState>(
        listener: (context, state) {
      // print(' register state : - ${state}');

      if (state is SellerRegErrorState) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: const Text("Login failed."),
            backgroundColor: Colors.red,
          ),
        );
      }
      if (state is SellerRegSuccessState) {
        navigationPush(context, SellerHomeScr());
      }

      if (state is SelOtpSendSuccessState) {
        print('this is otp State $state');
        // navigationPush(context, SellerHomeScr());
      }

      if (state is SelOtpResendSuccessState) {
        print('this is otp State $state');
        // navigationPush(context, SellerHomeScr());
      }
    }, child: BlocBuilder<SellerregconfBloc, SellerregconfState>(
            builder: (context, state) {
      return SellerSignUpForm();
    }));
  }
}

/* -------------------------------------------------------------------------- */
/*                     // ! 2. SIGNUP FORM WITHOUT BLOC LOGIC                    */
/* -------------------------------------------------------------------------- */
class SellerSignUpForm extends StatefulWidget {
  SellerSignUpForm({
    Key? key,
  }) : super(key: key);

  @override
  _SellerSignUpFormState createState() => _SellerSignUpFormState();
}

class _SellerSignUpFormState extends State<SellerSignUpForm> {
  dynamic gValue = '1';
  dynamic gdayValue = '1';
  dynamic modDelValue = 'Delivery';

  final _form = GlobalKey<FormState>();
  final fnameController = TextEditingController();
  final lnameController = TextEditingController();
  final phoneController = TextEditingController();
  final dateController = TextEditingController();
  final streetController = TextEditingController();
  final cityController = TextEditingController();
  final stateController = TextEditingController();
  final zipCodeController = TextEditingController();
  final countryController = TextEditingController();

  dynamic fodperCon;
  dynamic govIdCon;
  // File? fodperCon;
  // File? govIdCon;

  final time1Controller = TextEditingController();
  final time2Controller = TextEditingController();
  final expController = TextEditingController();
  final refferalController = TextEditingController();
  final hearController = TextEditingController();
  final intersetController = TextEditingController();
  final specialController = TextEditingController();
  // final delmodeCon = TextEditingController();
  /* -------------------------------------------------------------------------- */
  /*                      //  !SUBMIT MEHTOD FOR VALIDATION                     */
  /* -------------------------------------------------------------------------- */
  File? _file;

  @override
  Widget build(BuildContext context) {
    // ! rister method
    _registerNow() async {
      var isvalid = _form.currentState!.validate();
      if (!isvalid) {
        return 'Please Enter Valid Data';
      }
      _form.currentState!.save();

      // SellerRegModel regMod = new SellerRegModel.fromMap({
      //   "fname": fnameController.text,
      //   "lname": lnameController.text,
      //   "phone": phoneController.text,
      //   "dateC": dateController.text,
      //   "street": streetController.text,
      //   "city": cityController.text,
      //   "state": stateController.text,
      //   "zipCode": zipCodeController.text,
      //   "country": countryController.text,
      //   "fp": gValue,
      //   "fodper": await MultipartFile.fromPath('Photos', fodperCon),
      //   "govId": await MultipartFile.fromPath('Photos', fodperCon),
      //   " special": specialController.text,
      //   "dayC": gdayValue,
      //   "time1": time1Controller.text,
      //   "time2": time2Controller.text,
      //   "exp": expController.text,
      //   "hear": hearController.text,
      //   "interset": intersetController.text,
      //   "delMode": modDelValue,
      //   "refferal": refferalController.text,
      //   "otpD": '1'
      // });
      var isregis = await BlocProvider.of<SellerregconfBloc>(context)
        ..add(SelRegSaveBtnEvent(
            // SelRegSaveBtnEvent(regModel: regMod));
            fname: fnameController.text,
            lname: lnameController.text,
            phone: phoneController.text,
            dateC: dateController.text,
            street: streetController.text,
            city: cityController.text,
            state: stateController.text,
            zipCode: zipCodeController.text,
            country: countryController.text,
            fp: gValue,
            fodper: fodperCon,
            govId: govIdCon,
            // fodper: await MultipartFile.fromPath('Photos', fodperCon),
            // govId: await MultipartFile.fromPath('Photos', fodperCon),
            special: specialController.text,
            dayC: gdayValue,
            time1: time1Controller.text,
            time2: time2Controller.text,
            exp: expController.text,
            hear: hearController.text,
            interset: intersetController.text,
            delMode: modDelValue,
            refferal: refferalController.text,
            OtpD: '1'));

      // print(fnameController.text);
      // print(lnameController.text);
      // print(phoneController.text);
      // print(dateController.text);
      // print(streetController.text);
      // print(cityController.text);
      // print(stateController.text);
      // print(zipCodeController.text);
      // print(countryController.text);

      // print('this is Gvalue ${gValue}');

      // print(time1Controller.text);
      // print(time2Controller.text);
      // print(expController.text);
      // print(refferalController.text);
      // print(hearController.text);
      // print(intersetController.text);
      // print(specialController.text);
      // print('This is food per $fodperCon');
      // print('This is food per $govIdCon');
    }
    // ! end register method

    return Padding(
      padding: EdgeInsets.only(right: 5.0, left: 5.0, top: 5.0),
      child: Form(
        key: _form,
        child: Column(children: [
          /* -------------------------------------------------------------------------- */
          /*                                 FORM FIELD   part 1                                */
          /* -------------------------------------------------------------------------- */
          Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //! NameEdit text
                SellerTwoField(
                  hT1: 'FirstName',
                  hT2: 'LastName',
                  placeholder1: 'pawan',
                  placeholder2: 'kumar',
                  inputType1: TextInputType.name,
                  inputType2: TextInputType.name,
                  controller1: fnameController,
                  controller2: lnameController,
                  fNumber: 140,
                  // formVald1: validateName,
                  // formVald2: validateName,
                ),
                SelTxtLine('Phone (To sechedule interview)'),
                SellerEditFormFields(
                  placeholder: '9830993177',
                  inputType: TextInputType.phone,
                  controller: phoneController,
                  // formValidator: validateMobile,
                  sufIcon: 'yes',
                  sufIWid: Padding(
                    padding: const EdgeInsets.only(bottom: 1.0),
                    child: InkWell(
                      onTap: () {
                        // setState(() {
                        BlocProvider.of<SellerregconfBloc>(context)
                          ..add(SelRegOtpBtnEvent(
                            phone: phoneController.text,
                          ));
                        // });

                        navigationPush(
                            context, AlrtBox(phone: phoneController.text));
                      },
                      child: Text('verify',
                          style: TextStyle(fontSize: 15, color: redColor)),
                    ),
                  ),
                ),
                // ! time for meet
                SelTxtLine('Best Time for Contact'),
                SellerEditFormFields(
                    placeholder: '02:4:2021',
                    controller: dateController,
                    inputType: TextInputType.datetime,
                    sufIcon: 'yes',
                    sufI: Icons.access_time_sharp,
                    timeName: 'Yes'),

                // ! address
                SelTxtLine('Home Address (where you cook)'),

                SellerEditFormFields(
                  placeholder: 'Street',
                  controller: streetController,
                  inputType: TextInputType.streetAddress,
                  // formValidator: validateAddress,
                ),
                Row(
                  children: [
                    SellerEditFormFields(
                      placeholder: 'City',
                      inputType: TextInputType.text,
                      controller: cityController,
                      fNumber: 140.0,
                      // formValidator: validateAddress,
                    ),
                    SellerEditFormFields(
                      placeholder: 'State',
                      controller: stateController,
                      inputType: TextInputType.text,
                      fNumber: 140.0,
                      // formValidator: validateAddress,
                    ),
                  ],
                ),

                Row(
                  children: [
                    SellerEditFormFields(
                      placeholder: 'ZipCode',
                      inputType: TextInputType.number,
                      controller: zipCodeController,
                      fNumber: 140.0,
                      // formValidator: validateZipCode,
                    ),
                    SellerEditFormFields(
                      placeholder: 'Country',
                      controller: countryController,
                      inputType: TextInputType.text,
                      fNumber: 140.0,
                      // formValidator: validateAddress,
                    ),
                  ],
                ),

                SelTxtLine('Food Permit'),
                Padding(
                  padding: const EdgeInsets.only(right: 1.5, left: 3),
                  child: Row(
                    children: [
                      SellerRadBtn(
                        title: 'Yes',
                        val: '1',
                        gValue: gValue,
                        onChg: (ind) {
                          setState(() {
                            gValue = ind;
                          });
                        },
                      ),
                      SellerRadBtn(
                        title: 'No',
                        val: '2',
                        gValue: gValue,
                        onChg: (dynamic ind) {
                          setState(() {
                            gValue = ind;
                          });
                        },
                      ),
                    ],
                  ),
                ),
                // // ! food certicate
                Padding(
                  padding: const EdgeInsets.only(bottom: 3),
                  child: Row(
                    children: [
                      SellerUpdBtn(
                        title: 'Food Permit',
                        fileId:
                            // selectFileD(fileD: fodperCon)

                            () async {
                          final result = await FilePicker.platform
                              .pickFiles(allowMultiple: false);

                          if (result == null) return;
                          final fpath = result.files.single.path;

                          setState(() {
                            _file = File(fpath!);
                            fodperCon = _file;
                          });
                        },
                      ),
                      SellerUpdBtn(
                          title: 'Govt Id',
                          fileId:
                              //  selectFileD(fileD: govIdCon)

                              () async {
                            // ! Select files
                            final result = await FilePicker.platform
                                .pickFiles(allowMultiple: false);

                            if (result == null) return;
                            final fpath = result.files.single.path!;

                            setState(() {
                              _file = File(fpath);

                              govIdCon = _file;
                              // govIdCon = basename(_file!.path);
                            });
                          }),
                    ],
                  ),
                )
              ],
            ),
          ),

          /* -------------------------------------------------------------------------- */
          /*                             // END  FORM FIELD   PART 1                          */
          /* -------------------------------------------------------------------------- */
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SelTxtLine('Other Info'),
                  SelTxtLine('Cuisines you specialized in'),
                  // ! specialized food
                  SellerEditFormFields(
                    placeholder: 'Hydarbadi(indian)',
                    inputType: TextInputType.text,
                    controller: specialController,
                    // formValidator: validateField
                  ),
                  // ! Date
                  SelTxtLine('What day and time you cook ?'),

                  dayWork(),

                  // ! MODE OF DELIVERY

                  SelTxtLine('Mode of Delivery'),
                  modeofDel(
                      t1: 'Pickup',
                      t2: 'Delivery',
                      t3: 'Both',
                      val1: 'Pickup',
                      val2: 'Delivery',
                      val3: 'Both',
                      gVal: modDelValue,
                      onChg: (dynamic ind) {
                        setState(() {
                          modDelValue = ind;
                        });
                      }),

                  // ! time
                  Row(
                    children: [
                      SellerEditFormFields(
                        fNumber: 140.0,
                        placeholder: '10:00 AM',
                        // inputType: TextInputType.text,
                        // timeName: 'Yes',
                        savedValue: (val) {
                          setState(() {
                            time1Controller.text = val;
                          });
                        },
                      ),
                      Text('TO', style: TextStyle(fontSize: 13)),
                      SellerEditFormFields(
                        fNumber: 140.0,
                        placeholder: '01:00 PM',
                        inputType: TextInputType.number,
                        // timeName: 'Yes',
                        savedValue: (val) {
                          setState(() {
                            time2Controller.text = val;
                          });
                        },
                      ),
                    ],
                  ),

                  // ! Experince
                  SelTxtLine('How many years of experince in cooking'),
                  SellerEditFormFields(
                    placeholder: '05 years',
                    inputType: TextInputType.number,
                    controller: expController,
                  ),

                  // ! Refferal code
                  SellerTwoField(
                      hT1: 'How did you hear about us',
                      hT2: 'Refferal Code',
                      placeholder1: 'from friends',
                      inputType1: TextInputType.text,
                      controller1: hearController,
                      placeholder2: 'CA987SNMY',
                      controller2: refferalController,
                      inputType2: TextInputType.text,
                      fNumber: 120.0),

                  // !    Interset
                  SelTxtLine('What Interset you have'),
                  SellerEditFormFields(
                    placeholder: 'Earn Money',
                    inputType: TextInputType.text,
                    controller: intersetController,
                    // formValidator: validateField
                  ),
                ],
              ),
            ),
          ),

          /* -------------------------------------------------------------------------- */
          /*                                SUBMIT BUTTON   Start                             */
          /* -------------------------------------------------------------------------- */

          SellerSingleBtn(
            'Save',
            submitMethod: _registerNow,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 3.0, bottom: 7),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [SelTCSTxt('See'), SelTCUTxt('Terms & Condtions')],
            ),
          )
          /* -------------------------------------------------------------------------- */
          /*                          END  SUBMIT BUTTON                              */
          /* -------------------------------------------------------------------------- */
        ]),
      ),
    );
  }

  // ! Radio Btn
  Widget dayWork() {
    return Padding(
      padding: const EdgeInsets.only(left: 3.0, right: 5, top: 3),
      child: Container(
        child: SizedBox(
          height: 40,
          width: 500,
          child: ListView(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            children: [
              SellerRadBtn(
                title: 'Sun',
                val: '1',
                gValue: gdayValue,
                onChg: (dynamic ind) {
                  setState(() {
                    gdayValue = ind;
                  });
                },
              ),
              SellerRadBtn(
                title: 'Mon',
                val: '2',
                gValue: gdayValue,
                onChg: (dynamic ind) {
                  setState(() {
                    gdayValue = ind;
                  });
                },
              ),
              SellerRadBtn(
                title: 'Tue',
                val: '3',
                gValue: gdayValue,
                onChg: (dynamic ind) {
                  setState(() {
                    gdayValue = ind;
                  });
                },
              ),
              SellerRadBtn(
                title: 'Wed',
                val: '4',
                gValue: gdayValue,
                onChg: (dynamic ind) {
                  setState(() {
                    gdayValue = ind;
                    // dayController.text = ind;
                  });
                },
              ),
              SellerRadBtn(
                title: 'Thu',
                val: '5',
                gValue: gdayValue,
                onChg: (dynamic ind) {
                  setState(() {
                    gdayValue = ind;
                    // dayController.text = ind;
                  });
                },
              ),
              SellerRadBtn(
                title: 'Fri',
                val: '6',
                gValue: gdayValue,
                onChg: (dynamic ind) {
                  setState(() {
                    gdayValue = ind;
                    // dayController.text = ind;
                  });
                },
              ),
              SellerRadBtn(
                title: 'Sat',
                val: '7',
                gValue: gdayValue,
                onChg: (dynamic ind) {
                  setState(() {
                    gdayValue = ind;
                    // dayController.text = ind;
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget modeofDel({
    required String t1,
    required String t2,
    required String t3,
    dynamic val1,
    dynamic val2,
    dynamic val3,
    dynamic gVal,
    Function(dynamic)? onChg,
  }) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 3.0,
        right: 3,
      ),
      child: Container(
        child: SizedBox(
          height: 50,
          width: 500,
          child: ListView(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            children: [
              SellerRadBtn(
                title: t1,
                val: val1,
                gValue: gVal,
                onChg: onChg,
              ),
              SellerRadBtn(
                title: t2,
                val: val2,
                gValue: gVal,
                onChg: onChg,
              ),
              SellerRadBtn(
                title: t3,
                val: val3,
                gValue: gVal,
                onChg: onChg,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
