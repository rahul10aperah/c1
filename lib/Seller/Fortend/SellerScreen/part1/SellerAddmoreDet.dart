import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/SellerBtn.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/SellerConTxt.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/SellerFormField.dart';
import 'package:flutter/material.dart';

class SellerAddMoreDetails extends StatelessWidget {
  static const routeName = '/sellerAddMoreDetails';
  SellerAddMoreDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(' Add More Food Item'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //  ! REGISTER PAGE
                SellerItemDetForm()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                             // ! Food Item form                            */
/* -------------------------------------------------------------------------- */
class SellerItemDetForm extends StatefulWidget {
  SellerItemDetForm({Key? key}) : super(key: key);

  @override
  _SellerItemDetFormState createState() => _SellerItemDetFormState();
}

class _SellerItemDetFormState extends State<SellerItemDetForm> {
  final _form = GlobalKey<FormState>();
  final date1Con = TextEditingController();
  final date2Con = TextEditingController();
  final time1Con = TextEditingController();
  final time2Con = TextEditingController();
  final avaibleCon = TextEditingController();
  final limitCon = TextEditingController();

  final detailsCon = TextEditingController();

  /* -------------------------------------------------------------------------- */
  /*                      //  !SUBMIT MEHTOD FOR VALIDATION                     */
  /* -------------------------------------------------------------------------- */

  // ignore: unused_element
  _sellerDetMet() async {
    var isvalid = _form.currentState!.validate();
    if (!isvalid) {
      return 'Please Enter Valid Data';
    }
    _form.currentState!.save();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 5.0, left: 5.0, top: 8.0),
      child: Form(
        key: _form,
        child: Column(children: [
          /* -------------------------------------------------------------------------- */
          /*                                 FORM FIELD                                   */
          /* -------------------------------------------------------------------------- */

          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // ! Date

                  Row(
                    children: [
                      SelTxtLine('Select Date'),
                      SellerEditFormFields(
                        labelText: 'From',
                        placeholder: '4-May',
                        inputType: TextInputType.datetime,
                        sufIcon: 'yes',
                        sufI: Icons.calendar_today,
                        controller: date1Con,
                        fNumber: 100.0,
                      ),
                      Icon(Icons.remove, size: 15, color: Colors.black),
                      SellerEditFormFields(
                          labelText: 'To',
                          placeholder: '8-May',
                          inputType: TextInputType.datetime,
                          sufIcon: 'yes',
                          sufI: Icons.calendar_today,
                          controller: date2Con,
                          fNumber: 100.0),
                    ],
                  ),

                  // ! SERVING TIMING

                  Row(
                    children: [
                      SelTxtLine('Select Timing'),
                      SellerEditFormFields(
                        placeholder: '   12 AM',
                        inputType: TextInputType.number,
                        controller: time1Con,
                        fNumber: 100.0,
                      ),
                      Text(
                        'To',
                        style: TextStyle(fontSize: 15),
                      ),
                      SellerEditFormFields(
                        placeholder: '   2 PM',
                        inputType: TextInputType.number,
                        controller: time2Con,
                        fNumber: 100.0,
                      ),
                    ],
                  ),

                  // ! Serving aviable
                  Row(
                    children: [
                      SelTxtLine('Serving available'),
                      SellerEditFormFields(
                        placeholder: '   25',
                        inputType: TextInputType.number,
                        controller: avaibleCon,
                        fNumber: 100.0,
                      ),
                    ],
                  ),

                  // ! Order limit
                  Row(
                    children: [
                      SelTxtLine('Order Limit'),
                      SellerEditFormFields(
                        placeholder: '   5',
                        inputType: TextInputType.number,
                        controller: limitCon,
                        fNumber: 100.0,
                      ),
                    ],
                  ),

                  // ! HighLiht THings

                  SellerEditFormFields(
                    labelText: 'Edit more details',
                    placeholder: 'Edit more details',
                    inputType: TextInputType.text,
                    controller: detailsCon,
                    sufIcon: 'yes',
                    sufI: Icons.arrow_drop_down_rounded,
                    fNumber: 200.0,
                  ),
                ],
              ),
            ),
          ),

          /* -------------------------------------------------------------------------- */
          /*                                SUBMIT BUTTON   Start                             */
          /* -------------------------------------------------------------------------- */

          SellerSingleBtn(
            'Save',
            submitMethod: _sellerDetMet,
          ),
          /* -------------------------------------------------------------------------- */
          /*                          END  SUBMIT BUTTON                              */
          /* -------------------------------------------------------------------------- */

          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SelTCSTxt('By submitting you agree to our', fSize: 11.0),
              SelTCUTxt('Terms & Condtions', fSize: 11.0),
              SelTCSTxt('and', fSize: 11.0),
              SelTCUTxt('Privacy Statment', fSize: 11.0)
            ],
          )
        ]),
      ),
    );
  }

  // // ! HEADLINE

}
