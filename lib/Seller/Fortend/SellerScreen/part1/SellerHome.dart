import 'package:comp3/Seller/Backend/BlocPattern/HomeB/selhomep_bloc.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/Menu/menu.dart';
import 'package:comp3/util/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SellerHomeScr extends StatefulWidget {
  SellerHomeScr({Key? key}) : super(key: key);

  @override
  _SellerHomeScrState createState() => _SellerHomeScrState();
}

class _SellerHomeScrState extends State<SellerHomeScr> {
  dynamic selectedIndex = 'Available';
//  ! Select Menu Index Method
  void onItemTapped(String? index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Kitchen'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // ! MENU HEADER

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: double.infinity,
                    child: Row(
                      children: [
                        SellerMenuTxt('Available',
                            selectedIndex: selectedIndex,
                            onItemTapped: onItemTapped),
                        SellerMenuTxt('Expired',
                            selectedIndex: selectedIndex,
                            onItemTapped: onItemTapped)
                      ],
                    ),
                  ),
                ),

                SellerProdItem()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                          // !Seller Home Page                         */
/* -------------------------------------------------------------------------- */
class SellerHomeOne extends StatelessWidget {
  SellerHomeOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SelhomepBloc, SelhomepState>(
        listener: (context, state) {
      // print(' register state : - ${state}');

      if (state is SelHomeErrorState) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: const Text("Login failed."),
            backgroundColor: Colors.red,
          ),
        );
      }
      if (state is SelHomeSuccessState) {
        navigationPush(context, SellerHomeScr());
      }
    }, child:
            BlocBuilder<SelhomepBloc, SelhomepState>(builder: (context, state) {
      return SellerProdItem();
    }));
  }
}

// ! SellerProdItem HEADER
class SellerProdItem extends StatefulWidget {
  const SellerProdItem({Key? key}) : super(key: key);

  @override
  _SellerProdItemState createState() => _SellerProdItemState();
}

class _SellerProdItemState extends State<SellerProdItem> {
  List<bool> isSel = [true, false];
  dynamic clBtn = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: 2,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return Card(
                child: Container(
              child: Column(
                // shrinkWrap: true,
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        clBtn = true;
                      });
                    },
                    onDoubleTap: () {
                      setState(() {
                        clBtn = false;
                      });
                    },
                    child: Container(
                      //   // duration: Duration(seconds: 3),
                      width: double.infinity,
                      height: 150,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('images/COUPON.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),

                      child: Stack(
                        fit: StackFit.passthrough,
                        children: [
                          Positioned(
                            top: 10,
                            right: 20,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                    height: 25,
                                    width: 96,
                                    color: Colors.white,
                                    child: tglBtn()),
                              ],
                            ),
                          ),
                          // ! Card Item
                          Container(
                              width: double.infinity,
                              height: 50,
                              child: clickBtn())
                        ],
                      ),
                    ),
                  ),

                  // ! Deatils of Item
                  det('Title', 'Status', col: Colors.green),
                ],
              ),
            ));
          }),
    );
  }

  // ! detail HEADER
  Widget det(String t1, String t2, {Color? col}) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(t1, style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blueAccent),
                color: col,
              ),
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text(t2,
                    style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              ))
        ],
      ),
    );
  }

  // ! toggle btn
  Widget tglBtn() {
    return ToggleButtons(
      color: Colors.white,
      selectedColor: Colors.blue,
      fillColor: Colors.lightBlue.shade900,
      renderBorder: false,
      children: [
        Text(
          'on',
          textAlign: TextAlign.end,
          style: TextStyle(fontSize: 12, color: Colors.black),
        ),
        Text(
          'off',
          textAlign: TextAlign.end,
          style: TextStyle(fontSize: 12, color: Colors.black),
        ),
      ],
      isSelected: isSel,
      onPressed: (int newindex) {
        setState(() {
          for (int index = 0; index < isSel.length; index++) {
            if (index == newindex) {
              isSel[index] = true;
            } else {
              isSel[index] = false;
            }
          }
        });
      },
    );
  }

  Widget? clickBtn() {
    if (clBtn == true) {
      return Padding(
        padding:
            const EdgeInsets.only(top: 50.0, bottom: 50, left: 10, right: 10),
        child: Container(
          width: double.infinity,
          height: 50,
          color: Colors.red,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              eadTxt('Edit'),
              eadTxt('Add/Extend Time'),
              eadTxt('Delete'),
            ],
          ),
        ),
      );
    }
  }

  // ! Text for EAD BTN
  Widget eadTxt(String t) {
    return Expanded(
      child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
          ),
          child: Text(t,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 15, color: Colors.white))),
    );
  }
}
