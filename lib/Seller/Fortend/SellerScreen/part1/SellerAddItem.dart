import 'dart:io';

import 'package:comp3/Seller/Backend/BlocPattern/DishAdd/dishadd_bloc.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/SellerBtn.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/SellerFormField.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/Selleruplod.dart';
import 'package:comp3/util/common.dart';
import 'package:comp3/util/input_validation.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'SellerHome.dart';

class SellerAddItem extends StatelessWidget {
  static const routeName = '/SellerAddItem';
  SellerAddItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(' Add New Food Item'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //  ! REGISTER PAGE
                // SellerAddItemForm()
                DishAddedOne()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                          // ! REGISTER PAGE LOGIC                          */
/* -------------------------------------------------------------------------- */
class DishAddedOne extends StatelessWidget {
  DishAddedOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<DishaddBloc, DishaddState>(listener: (context, state) {
      // print(' register state : - ${state}');

      if (state is DishAddedErrorState) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: const Text("Added Dish."),
            backgroundColor: Colors.red,
          ),
        );
      }
      if (state is DishAddedSuccessState) {
        navigationPush(context, SellerHomeScr());
      }
    }, child: BlocBuilder<DishaddBloc, DishaddState>(builder: (context, state) {
      return SellerAddItemForm();
    }));
  }
}

/* -------------------------------------------------------------------------- */
/*                             // ! Food Item form                            */
/* -------------------------------------------------------------------------- */
class SellerAddItemForm extends StatefulWidget {
  SellerAddItemForm({Key? key}) : super(key: key);

  @override
  _SellerAddItemFormState createState() => _SellerAddItemFormState();
}

class _SellerAddItemFormState extends State<SellerAddItemForm> {
  dynamic gValue = 'Veg';
  dynamic gdayValue = 'Mild';
  final _form = GlobalKey<FormState>();
  final dishCon = TextEditingController();
  final desCon = TextEditingController();
  final priceCon = TextEditingController();
  final cusinCon = TextEditingController();
  final dietCon = TextEditingController();
  final spicyCon = TextEditingController();
  final allerCon = TextEditingController();
  final makCon = TextEditingController();
  final catCon = TextEditingController();
  final preptCon = TextEditingController();
  final highCon = TextEditingController();
  final disPertCon = TextEditingController();
  final disPriceCon = TextEditingController();
  dynamic img1;
  dynamic img2;
  dynamic img3;
  dynamic img4;

  File? _file;
  // // ! Select Filed

  /* -------------------------------------------------------------------------- */
  /*                      //  !SUBMIT MEHTOD FOR VALIDATION                     */
  /* -------------------------------------------------------------------------- */

  // ignore: unused_element
  _sellerItemMetod() async {
    var isvalid = _form.currentState!.validate();
    if (!isvalid) {
      return 'Please Enter Valid Data';
    }
    _form.currentState!.save();
    var isregis = await BlocProvider.of<DishaddBloc>(context)
      ..add(DishAddedBtnEvent(
        dName: dishCon.text,
        dDesc: desCon.text,
        price: priceCon.text,
        disPer: disPertCon.text,
        disprice: disPriceCon.text,
        cusi: cusinCon.text,
        diet: dietCon.text,
        spicy: spicyCon.text,
        allerg: allerCon.text,
        made: makCon.text,
        cate: catCon.text,
        pretime: preptCon.text,
        hightl: highCon.text,
        img1: img1,
        img2: img2,
        img3: img3,
        img4: img4,
      ));

    print('img1   $img1');
    print('dname ${dishCon.text}');
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 5.0, left: 5.0, top: 10.0),
      child: Form(
        key: _form,
        child: Column(children: [
          /* -------------------------------------------------------------------------- */
          /*                                 FORM FIELD                                   */
          /* -------------------------------------------------------------------------- */

          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SelTxtLine('Add Images'),
                  SizedBox(
                    height: 50,
                    width: 500,
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: [
                        SelUpdMultiFile(upDFile: () async {
                          final result = await FilePicker.platform
                              .pickFiles(allowMultiple: true);

                          if (result == null) return;
                          final fpath = result.files.single.path!;

                          setState(() {
                            _file = File(fpath);
                            img1 = _file;
                          });
                        }),
                        SelUpdMultiFile(upDFile: () async {
                          final result = await FilePicker.platform
                              .pickFiles(allowMultiple: true);

                          if (result == null) return;
                          final fpath = result.files.single.path!;

                          setState(() {
                            _file = File(fpath);
                            _file = img2;
                          });
                        }),
                        SelUpdMultiFile(upDFile: () async {
                          final result = await FilePicker.platform
                              .pickFiles(allowMultiple: true);

                          if (result == null) return;
                          final fpath = result.files.single.path!;

                          setState(() {
                            _file = File(fpath);
                            img3 = _file;
                          });
                        }),
                        SelUpdMultiFile(upDFile: () async {
                          final result = await FilePicker.platform
                              .pickFiles(allowMultiple: true);

                          if (result == null) return;
                          final fpath = result.files.single.path!;

                          setState(() {
                            _file = File(fpath);
                            img4 = _file;
                          });
                        }),
                      ],
                    ),
                  ),
                  // ! Field food
                  SelTxtLine('Dish Name'),
                  SellerEditFormFields(
                    placeholder: 'Hydarbadi(indian)',
                    inputType: TextInputType.name,
                    controller: dishCon,
                    // formValidator: validateField,
                  ),
                  SelTxtLine('Dish Description'),
                  SellerEditFormFields(
                    placeholder: 'this discription is',
                    inputType: TextInputType.text,
                    controller: desCon,
                    // formValidator: validateDescription,
                  ),

                  // ! Row Filed for Price and Cusines
                  SellerTwoField(
                    hT1: 'Price',
                    hT2: 'Cuisines',
                    // formVald1: validateField,
                    // formVald2: validateField,
                    placeholder1: '80',
                    inputType1: TextInputType.number,
                    controller1: priceCon,
                    placeholder2: 'Chicken Briyan',
                    inputType2: TextInputType.text,
                    controller2: cusinCon,
                    fNumber: 140,
                  ),

                  // ! discount price and perctnage
                  SellerTwoField(
                    hT1: 'DiscountPrice',
                    hT2: 'DiscountPercentage',
                    // formVald1: validateField,
                    // formVald2: validateField,
                    placeholder1: '500',
                    inputType1: TextInputType.number,
                    controller1: disPriceCon,
                    placeholder2: '5',
                    inputType2: TextInputType.number,
                    controller2: disPertCon,
                    fNumber: 140,
                  ),

                  // ! radio btn Dietary and spicy Level
                  SelTxtLine('Dietary'),
                  dayWork(
                      t1: 'Veg',
                      t2: 'Non-Veg',
                      t3: 'Vegan',
                      val1: 'Veg',
                      val2: 'Non-Veg',
                      val3: 'Vegan',
                      gVal: gValue,
                      onChg: (dynamic ind) {
                        setState(() {
                          gValue = ind;
                          dietCon.text = ind;
                        });
                      }),
                  //  ! spicy Level
                  SelTxtLine('Spicy Level'),
                  dayWork(
                      t1: 'Mild',
                      t2: 'Medium',
                      t3: 'Hot',
                      val1: 'Mild',
                      val2: 'Medium',
                      val3: 'Hot',
                      gVal: gdayValue,
                      onChg: (dynamic ind) {
                        setState(() {
                          gdayValue = ind;
                          spicyCon.text = ind;
                        });
                      }),

                  // ! allery and Making
                  SellerTwoField(
                    hT1: 'Allergens',
                    hT2: 'Making',
                    // formVald1: validateField,
                    // formVald2: validateField,
                    placeholder1: 'Nuts',
                    inputType1: TextInputType.text,
                    controller1: allerCon,
                    placeholder2: 'Halaal',
                    controller2: makCon,
                    inputType2: TextInputType.text,
                    fNumber: 140,
                  ),

                  // ! Category and Perpartion Time
                  SellerTwoField(
                    hT1: 'Category',
                    hT2: 'Perparation Time',
                    placeholder1: 'Lunch',
                    // formVald1: validateField,
                    // formVald2: validateField,
                    inputType1: TextInputType.text,
                    controller1: catCon,
                    sufIcon: 'yes',
                    sufI: Icons.arrow_drop_down_circle,
                    placeholder2: '3 Hours',
                    controller2: preptCon,
                    inputType2: TextInputType.number,
                    sufIcon2: 'yes',
                    sufI2: Icons.arrow_drop_down_circle,
                    fNumber: 140,
                  ),

                  // ! HighLiht THings
                  SelTxtLine('Any other things to heighlight'),
                  SellerEditFormFields(
                    placeholder: 'Any other things to heighlight',
                    inputType: TextInputType.text,
                    controller: highCon,
                  ),
                ],
              ),
            ),
          ),

          /* -------------------------------------------------------------------------- */
          /*                                SUBMIT BUTTON   Start                             */
          /* -------------------------------------------------------------------------- */

          SellerSingleBtn(
            'Save',
            submitMethod: _sellerItemMetod,
          )
          /* -------------------------------------------------------------------------- */
          /*                          END  SUBMIT BUTTON                              */
          /* -------------------------------------------------------------------------- */
        ]),
      ),
    );
  }

  Widget dayWork({
    required String t1,
    required String t2,
    required String t3,
    dynamic val1,
    dynamic val2,
    dynamic val3,
    dynamic gVal,
    Function(dynamic)? onChg,
  }) {
    return Padding(
      padding: const EdgeInsets.only(left: 3.0, right: 5, top: 3),
      child: Container(
        child: SizedBox(
          height: 45,
          width: 500,
          child: ListView(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            children: [
              SellerRadBtn(
                title: t1,
                val: val1,
                gValue: gVal,
                onChg: onChg,
              ),
              SellerRadBtn(
                title: t2,
                val: val2,
                gValue: gVal,
                onChg: onChg,
              ),
              SellerRadBtn(
                title: t3,
                val: val3,
                gValue: gVal,
                onChg: onChg,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
