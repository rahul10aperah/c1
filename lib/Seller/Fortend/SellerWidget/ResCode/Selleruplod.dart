import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:path/path.dart';
import 'package:flutter/material.dart';

import 'SellerFormField.dart';

class SellerUpdBtn extends StatefulWidget {
  final String title;
  dynamic fileId;

  SellerUpdBtn({
    Key? key,
    required this.title,
    this.fileId,
  }) : super(key: key);

  @override
  _SellerUpdBtnState createState() => _SellerUpdBtnState();
}

class _SellerUpdBtnState extends State<SellerUpdBtn> {
  File? _file;
//  final GlobalKey<ScaffoldState> _scaffoldstate = new GlobalKey<ScaffoldState>();

  // selectFile() async {
  //   final result = await FilePicker.platform.pickFiles(allowMultiple: false);

  //   if (result == null) return;
  //   final fpath = result.files.single.path!;

  //   setState(() {
  //     _file = File(fpath);
  //     uplodFile();
  //   });
  // }

  // uplodFile() {
  //   if (_file == null) return;
  //   final filen = basename(_file!.path);
  //   final dest = "files/$filen";
  //   setState(() {
  //     widget.fileId = dest;
  //   });

  //   print(filen);
  //   print(dest);
  // }

//

  @override
  Widget build(BuildContext context) {
    // ! file name base
    final filename = _file != null ? basename(_file!.path) : 'No Files';

    return Padding(
      padding: const EdgeInsets.only(left: 1.0),
      child: Container(
        child: Row(children: [
          SelTxtLine(widget.title),
          Column(
            children: [
              MaterialButton(
                  color: Colors.blue,
                  child: Text("Upload", style: TextStyle(color: Colors.white)),
                  // onPressed: selectFile
                  onPressed: widget.fileId),
              // Text(
              //   filename,
              //   style: TextStyle(
              //     fontSize: 4,
              //   ),
              //   maxLines: 1,
              // ),
            ],
          )
        ]),
      ),
    );
  }
}

/* -------------------------------------------------------------------------- */
/*                         // ! 2 UPLOAD MULTIPLE PIC                         */
/* -------------------------------------------------------------------------- */
class SelUpdMultiFile extends StatefulWidget {
  final String? title;
  dynamic upDFile;

  SelUpdMultiFile({
    Key? key,
    this.title,
    this.upDFile,
  }) : super(key: key);

  @override
  _SelUpdMultiFileState createState() => _SelUpdMultiFileState();
}

class _SelUpdMultiFileState extends State<SelUpdMultiFile> {
  File? _file;

  // selectFile() async {
  //   final result = await FilePicker.platform.pickFiles(allowMultiple: false);

  //   if (result == null) return;
  //   final fpath = result.files.single.path!;

  //   setState(() {
  //     _file = File(fpath);
  //   });
  // }

  // uplodFile() {
  //   if (_file == null) return;
  //   final filen = basename(_file!.path);
  //   final dest = "files/$filen";
  // }

//

  @override
  Widget build(BuildContext context) {
    // ! file name base
    final filename = _file != null ? basename(_file!.path) : 'No Files';

    return Padding(
        padding: const EdgeInsets.all(1.0),
        child: MaterialButton(
          color: Color.fromRGBO(215, 219, 221, .5),
          height: 45,
          onPressed: widget.upDFile,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          child: Ink(
            decoration: BoxDecoration(
              // ! Cricle Border
              borderRadius: BorderRadius.circular(5),
            ),
            child: Container(
              constraints: BoxConstraints(
                  maxWidth: 20.0,
                  minHeight: 45.0,
                  maxHeight: 45.0), // min sizes for Material buttons
              alignment: Alignment.center,
              child: Center(
                child: Container(
                  height: 20,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.red),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: Colors.red,
                  ),
                  child: Center(
                    child: Icon(
                      Icons.add,
                      size: 15,
                      color: Colors.white,
                      textDirection: TextDirection.ltr,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
