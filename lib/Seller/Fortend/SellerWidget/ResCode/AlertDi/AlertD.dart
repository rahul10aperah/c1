import 'dart:async';

import 'package:comp3/Elements/buttons.dart';
import 'package:comp3/Elements/input_widgets.dart';
import 'package:comp3/Seller/Backend/BlocPattern/Reg/sellerregconf_bloc.dart';
import 'package:comp3/Seller/Fortend/SellerWidget/ResCode/SellerFormField.dart';
import 'package:comp3/util/common.dart';
import 'package:comp3/util/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AlrtBox extends StatefulWidget {
  String? phone;
  AlrtBox({Key? key, this.phone
      // required this.cont
      })
      : super(key: key);

  @override
  _AlrtBoxState createState() => _AlrtBoxState();
}

class _AlrtBoxState extends State<AlrtBox> {
  late int _start;

  void startTimer() {
    _start = 59;
    const oneSec = const Duration(seconds: 1);
    Timer.periodic(
      oneSec,
      (Timer timer) {
        setState(() {
          if (_start == 0)
            timer.cancel();
          else
            _start--;
        });
      },
    );
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  List<String> otpValue = <String>[];

  bool buttonVisibility = false;

  TextEditingController otpController1 = TextEditingController();
  TextEditingController otpController2 = TextEditingController();
  TextEditingController otpController3 = TextEditingController();
  TextEditingController otpController4 = TextEditingController();
  TextEditingController otpController5 = TextEditingController();
  TextEditingController otpController6 = TextEditingController();

  final focusNode1 = FocusNode();
  final focusNode2 = FocusNode();
  final focusNode3 = FocusNode();
  final focusNode4 = FocusNode();
  final focusNode5 = FocusNode();
  final focusNode6 = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: AlertDialog(
          title: Text("Verfiy Phone"),
          content: Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    child: OtpTextField(
                  focusNode: focusNode1,
                  controller: otpController1,
                  // keyBoard: TextInputType.number,
                  inputFormatterData: digitsInputFormatter(size: 1),
                  onChanged: (String newVal) {
                    setState(() {
                      if (newVal.length == 0) {
                        otpController1.clear();
                        otpValue.removeAt(0);
                      }
                      if (newVal.length == 1) {
                        FocusScope.of(context).requestFocus(focusNode2);
                        otpValue.insert(0, newVal);
                        buttonVisibility = true;
                      }
                    });
                  },
                )),
                widthSizedBox(10.0),
                Expanded(
                    child: OtpTextField(
                  focusNode: focusNode2,
                  controller: otpController2,
                  // keyBoard: TextInputType.number,
                  inputFormatterData: digitsInputFormatter(size: 1),
                  onChanged: (String newVal) {
                    setState(() {
                      if (newVal.length == 0) {
                        otpController2.clear();
                        otpValue.removeAt(1);
                        FocusScope.of(context).requestFocus(focusNode1);
                      }
                      if (newVal.length == 1) {
                        FocusScope.of(context).requestFocus(focusNode3);
                        otpValue.insert(1, newVal);
                      }
                    });
                  },
                )),
                widthSizedBox(10.0),
                Expanded(
                    child: OtpTextField(
                  focusNode: focusNode3,
                  controller: otpController3,
                  // keyBoard: TextInputType.number,
                  inputFormatterData: digitsInputFormatter(size: 1),
                  onChanged: (String newVal) {
                    setState(() {
                      if (newVal.length == 0) {
                        otpController3.clear();
                        otpValue.removeAt(2);
                        FocusScope.of(context).requestFocus(focusNode2);
                      }
                      if (newVal.length == 1) {
                        FocusScope.of(context).requestFocus(focusNode4);
                        otpValue.insert(2, newVal);
                      }
                    });
                  },
                )),
                widthSizedBox(10.0),
                Expanded(
                    child: OtpTextField(
                  focusNode: focusNode4,
                  controller: otpController4,
                  // keyBoard: TextInputType.number,
                  inputFormatterData: digitsInputFormatter(size: 1),
                  onChanged: (String newVal) {
                    setState(() {
                      if (newVal.length == 0) {
                        otpController4.clear();
                        otpValue.removeAt(3);
                        FocusScope.of(context).requestFocus(focusNode3);
                      }
                      if (newVal.length == 1) {
                        FocusScope.of(context).requestFocus(focusNode5);
                        otpValue.insert(3, newVal);
                      }
                    });
                  },
                )),
                widthSizedBox(10.0),
                Expanded(
                    child: OtpTextField(
                  focusNode: focusNode5,
                  controller: otpController5,
                  // keyBoard: TextInputType.number,
                  inputFormatterData: digitsInputFormatter(size: 1),
                  onChanged: (String newVal) {
                    setState(() {
                      if (newVal.length == 0) {
                        otpController5.clear();
                        otpValue.removeAt(4);
                        FocusScope.of(context).requestFocus(focusNode4);
                      }
                      if (newVal.length == 1) {
                        FocusScope.of(context).requestFocus(focusNode6);
                        otpValue.insert(4, newVal);
                      }
                    });
                  },
                )),
                widthSizedBox(10.0),
                Expanded(
                    child: OtpTextField(
                  focusNode: focusNode6,
                  controller: otpController6,
                  // keyBoard: ,
                  inputFormatterData: digitsInputFormatter(size: 1),
                  onChanged: (String newVal) {
                    setState(() {
                      if (newVal.length == 0) {
                        otpController6.clear();
                        otpValue.removeAt(5);
                        FocusScope.of(context).requestFocus(focusNode5);
                      }
                      if (newVal.length == 1) {
                        otpValue.insert(5, newVal);
                      }
                    });
                  },
                )),
              ],
            ),
          ),
          actions: [
            // ! Button for varify
            // heightSizedBox(height / 10),
            Center(
              child: Container(
                width: 200,
                height: 50,
                child: BigButton(
                  title: 'Verify OTP',
                  onTap: () {
                    String otp = '';
                    for (String item in otpValue) {
                      otp += item;
                    }
                    if (otp.length == 6) {
                      // widget.emailMobile!.contains('@')
                      //     ? _con.user.email = widget.emailMobile
                      //     : _con.user.mobile = widget.emailMobile;
                      // _con.user.otp = otp;
                      // _con.verifyOTP(context, widget.isEmail
                      // );
                    } else {
                      snackBar(context, 'Please fill otp first' + otp);
                    }
                  },
                ),
              ),
            ),

            // ! DON'T receive message
            // heightSizedBox(height / 5),
            if (_start <= 0)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Didn't receive OTP?  ",
                    style: TextStyle(
                      fontSize: 13,
                      color: Colors.grey.shade600,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      BlocProvider.of<SellerregconfBloc>(context)
                        ..add(SelRegReOtpBtnEvent(
                          phone: widget.phone,
                        ));
                      // widget.isEmail
                      //     ? _con.reSendOTPMail(context, widget.emailMobile)
                      //     : _con.reSendOTPMbl(context, widget.emailMobile);
                      startTimer();
                    },
                    child: Text('Resend new OTP',
                        style: TextStyle(
                            fontSize: 13,
                            color: redColor,
                            decoration: TextDecoration.underline,
                            fontFamily: montserratMedium)),
                  ),
                ],
              )
            else
              Text(
                "00:${_start < 10 ? '0' : ''}$_start ",
                style: TextStyle(
                    fontSize: 13,
                    color: Colors.grey.shade600,
                    fontFamily: montserratMedium),
                textAlign: TextAlign.center,
              ),
          ],
        ),
      ),
    );
  }
}
