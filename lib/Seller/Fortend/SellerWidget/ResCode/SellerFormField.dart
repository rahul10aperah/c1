import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// ignore: must_be_immutable
class SellerEditFormFields extends StatefulWidget {
  final TextInputType? inputType;
  final String? placeholder;
  String? labelText;
  dynamic savedValue;
  String? initValue;
  final fNumber;
  final String? sufIcon;
  final IconData? sufI;
  Widget? sufIWid;
  final TextEditingController? controller;
  String? Function(String?)? formValidator;
  // final Function? formValidator;
  String? timeName;

  SellerEditFormFields({
    Key? key,
    this.fNumber,
    this.inputType,
    this.placeholder,
    this.controller,
    this.formValidator,
    this.labelText,
    this.savedValue,
    this.initValue,
    this.sufI,
    this.sufIcon,
    this.sufIWid,
    this.timeName,
  }) : super(key: key);

  @override
  _FieldFState createState() => _FieldFState();
}

class _FieldFState extends State<SellerEditFormFields> {
  // ignore: unused_element
  // ! choosing Date format
  Future _choseDate() async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: new TimeOfDay.now(),
      initialEntryMode: TimePickerEntryMode.dial,
    );
    if (picked != null && picked != TimeOfDay.now())
      setState(() {
        var selDate = picked;
        var dat = "${picked.hour}:${picked.minute}:${picked.hourOfPeriod}";

        widget.controller!.text = dat;
      });
  }

  // ! Drop Down Btn

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 5.0, right: 5.0, top: 2),
      child: SizedBox(
        height: 36,
        width: widget.fNumber != null ? widget.fNumber : double.infinity,
        child: TextFormField(
            onTap: () {
              if (widget.timeName == 'Yes') {
                _choseDate();
              }
            },
            initialValue: widget.initValue,
            controller: widget.controller,
            onSaved: widget.savedValue,
            keyboardType: widget.inputType,
            // validator: (value) => widget.formValidator!(value),
            validator: widget.formValidator,
            autofocus: false,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(5.0, 1.0, 5.0, 0),
                labelStyle: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
                labelText: widget.labelText,
                hintText: widget.placeholder,
                suffixIcon: widget.sufIcon == 'yes'
                    ? sufWid(sufIWid: widget.sufIWid)
                    : null)),
      ),
    );
  }

  // ! WIDGET FOR SUFFIX
  Widget? sufWid({Widget? sufIWid}) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        child: widget.sufIcon == 'yes' && widget.sufI != null
            ? IconButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  if (widget.placeholder == 'time') {
                    _choseDate();
                  }
                },
                icon: Icon(
                  widget.sufI,
                  color: Colors.grey,
                ),
              )
            : sufIWid);
  }
  // ! Icon Widget

}

// ! RADION BUTTON
// ignore: must_be_immutable
class SellerRadBtn extends StatefulWidget {
  dynamic gValue = 0;
  String title;
  dynamic val;
  Function(dynamic)? onChg;
  SellerRadBtn(
      {Key? key, required this.title, this.val, this.gValue, this.onChg})
      : super(key: key);

  @override
  _SellerRadBtnState createState() => _SellerRadBtnState();
}

class _SellerRadBtnState extends State<SellerRadBtn> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: -7,
          children: [
            Transform.scale(
              alignment: Alignment.centerLeft,
              scale: 1,
              child: Radio(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: widget.val,
                  groupValue: widget.gValue,
                  onChanged: widget.onChg),
            ),
            Text(
              widget.title.toString(),
              maxLines: 1,
              style: TextStyle(fontSize: 15),
              textDirection: TextDirection.ltr,
              textAlign: TextAlign.start,
            ),
          ]),
      // ),
    );
  }
}

// ! SELLER FORM IN HEADER TITLE AND FORM FIELD
class SellerTwoField extends StatelessWidget {
  final String hT1;
  final String hT2;
  final String? placeholder1;
  final TextInputType? inputType1;
  final TextEditingController? controller1;
  final String? placeholder2;
  final TextInputType? inputType2;
  final TextEditingController? controller2;
  final double? fNumber;
  final String? sufIcon;
  final IconData? sufI;
  final String? sufIcon2;
  final IconData? sufI2;
  String? Function(String?)? formVald1;
  // final Function? formVald1;
  // final Function? formVald2;
  String? Function(String?)? formVald2;

  SellerTwoField(
      {Key? key,
      required this.hT1,
      required this.hT2,
      this.placeholder1,
      this.controller1,
      this.inputType1,
      this.placeholder2,
      this.controller2,
      this.inputType2,
      this.sufIcon,
      this.sufI,
      this.sufIcon2,
      this.sufI2,
      this.formVald1,
      this.formVald2,
      this.fNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SelTxtLine(hT1),
          SellerEditFormFields(
            placeholder: placeholder1,
            inputType: inputType1,
            controller: controller1,
            fNumber: fNumber,
            sufIcon: sufIcon,
            sufI: sufI,
            formValidator: formVald1,
          ),
        ],
      ),
      // ! Seconf Field
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SelTxtLine(hT2),
          SellerEditFormFields(
            placeholder: placeholder2,
            inputType: inputType2,
            controller: controller2,
            fNumber: fNumber,
            sufIcon: sufIcon2,
            sufI: sufI2,
            formValidator: formVald2,
          ),
        ],
      )
    ]);
  }
}

// ! HEADLINE TEXT FOR LABLE
class SelTxtLine extends StatelessWidget {
  final String title;
  SelTxtLine(this.title, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, right: 5, top: 7),
      child: Text(
        title,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
      ),
    );
  }
}
