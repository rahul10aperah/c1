import 'package:flutter/material.dart';

class SellerSingleBtn extends StatelessWidget {
  final String btnName;
  final dynamic submitMethod;
  SellerSingleBtn(this.btnName, {Key? key, this.submitMethod})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, left: 15, right: 15, bottom: 5),
      child: MaterialButton(
        color: Colors.red,
        height: 60,
        onPressed: submitMethod,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        child: Ink(
          decoration: BoxDecoration(
            // ! Cricle Border
            borderRadius: BorderRadius.circular(5),
          ),
          child: Container(
            constraints: BoxConstraints(
                maxWidth: 230.0,
                minHeight: 40.0,
                maxHeight: 50.0), // min sizes for Material buttons
            alignment: Alignment.center,
            child: Text(
              btnName,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 20.0),
            ),
          ),
        ),
      ),
    );
  }
}
