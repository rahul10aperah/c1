import 'package:flutter/material.dart';

// ! Terms and Condtion Txt
// !1  Simple Text
class SelTCSTxt extends StatelessWidget {
  final String t;
  final double? fSize;
  final Color? fCol;
  final TextDecoration? fDec;
  SelTCSTxt(this.t, {Key? key, this.fCol, this.fDec, this.fSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(t,
        style: TextStyle(
            fontSize: fSize != null ? fSize : 12,
            color: fCol != null ? fCol : Colors.black));
  }
}

// ! Link Txt
class SelTCUTxt extends StatelessWidget {
  final String t;
  final dynamic fSize;
  final Color? fCol;
  final TextDecoration? fDec;
  SelTCUTxt(this.t, {Key? key, this.fCol, this.fDec, this.fSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Text(t,
          style: TextStyle(
            fontSize: fSize != null ? fSize : 12,
            decoration: fDec != null ? fDec : TextDecoration.underline,
            color: fCol != null ? fCol : Colors.blue,
          )),
    );
  }
}
