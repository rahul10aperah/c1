import 'package:flutter/material.dart';

// ignore: must_be_immutable
class SellerMenuBtn extends StatefulWidget {
  dynamic selectedIndex = 'Pending';
  final List? titList;

  SellerMenuBtn({
    Key? key,
    this.selectedIndex,
    this.titList,
  }) : super(key: key);

  @override
  _SellerMenuBtnState createState() => _SellerMenuBtnState();
}

class _SellerMenuBtnState extends State<SellerMenuBtn> {
  void onItemTapped(String? index) {
    setState(() {
      widget.selectedIndex = index;
    });
  }

  txtNum() {
    for (var i in widget.titList!) {
      return SellerMenuTxt(
        widget.titList![i].toString(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(children: txtNum()),
    );
    // SizedBox(
    //   height: 30,
    //   width: 400,
    //   child: ListView.builder(
    //     shrinkWrap: true,
    //     scrollDirection: Axis.horizontal,
    //     itemCount: widget.titList!.length,
    //     itemBuilder: (context, index) {
    //       return SellerMenuTxt(widget.titList![index].toString(),
    //           selectedIndex: widget.selectedIndex, onItemTapped: onItemTapped);
    //     },
    //   ),
    // );
  }
}

// ! TEXTBTN  FOR MENU
// ignore: must_be_immutable
class SellerMenuTxt extends StatefulWidget {
  final String t;
  dynamic selectedIndex;
  dynamic onItemTapped;

  SellerMenuTxt(
    this.t, {
    Key? key,
    this.selectedIndex,
    this.onItemTapped,
  }) : super(key: key);

  @override
  _SellerMenuTxtState createState() => _SellerMenuTxtState();
}

class _SellerMenuTxtState extends State<SellerMenuTxt> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.blueAccent),
          color: widget.selectedIndex == widget.t ? Colors.blue : null,
        ),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: InkWell(
            onTap: () {
              widget.onItemTapped(widget.t);
            },
            child: Text(widget.t,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                )),
          ),
        ),
      ),
    );
  }
}
