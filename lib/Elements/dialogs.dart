import 'package:comp3/Repository/user_repository.dart';
import 'package:comp3/Screens/onboarding.dart';
import 'package:comp3/util/common.dart';
import 'package:comp3/util/style.dart';
import 'package:flutter/material.dart';

Future<dynamic> logOutDialog(context) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(
          'Are you sure you want to \n LogOut',
          textAlign: TextAlign.center,
          style: TextStyle(fontFamily: montserratMedium, fontSize: 16),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('No', style: TextStyle(color: darkBlueColor)),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          TextButton(
            child: Text('Yes', style: TextStyle(color: Colors.red)),
            onPressed: () {
              logout().then(
                  (value) => navigationPushReplacement(context, OnBoarding()));
            },
          )
        ],
      );
    },
  );
}
