import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_place/google_place.dart';

import '../Elements/address_Item.dart';
import '../Model/address.dart';
import '../Screens/select_location.dart';
import '../util/common.dart';
import '../util/shared_helper.dart';
import '../util/style.dart';
import 'input_widgets.dart';

class AddressBottomSheet extends StatefulWidget {
  @override
  _AddressBottomSheetState createState() => _AddressBottomSheetState();
}

class _AddressBottomSheetState extends State<AddressBottomSheet> {
  GooglePlace? googlePlace;
  List<AutocompletePrediction> predictions = [];
  List<String> recentSearches = [];
  SharedHelper shared = SharedHelper();
  Address address = Address();

  @override
  void initState() {
    super.initState();
    // shared.remove('recentSearches');
    googlePlace = GooglePlace('AIzaSyDvHPQaJcUZp6WbYS7YOj0c6ziF2mQ0MU0');
    getRecentSearch();
  }

  getRecentSearch() async {
    recentSearches = await shared.getStringList('recentSearches');
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF71181C),
      height: getHeight(context) * 0.9,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15.0),
                topRight: Radius.circular(15.0))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            heightSizedBox(15.0),
            EditTextField(
              onChanged: (value) {
                if (value.isNotEmpty) {
                  autoCompleteSearch(value);
                } else {
                  if (predictions.length > 0 && mounted) {
                    setState(() => predictions = []);
                  }
                }
              },
              keyBoard: TextInputType.emailAddress,
              labelText: 'Search',
              hint: 'Enter address',
            ),
            heightSizedBox(10.0),
            Expanded(
              child: predictions.isNotEmpty
                  ? ListView.builder(
                      itemCount: predictions.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.zero,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 13, horizontal: 10),
                          margin: EdgeInsets.symmetric(vertical: 3),
                          decoration: boxDecoration.copyWith(
                            borderRadius: BorderRadius.all(Radius.circular(3)),
                          ),
                          child: InkWell(
                            onTap: () {
                              address.id = recentSearches.length.toString();
                              address.area = predictions[index].description;
                              recentSearches.insert(
                                  0, jsonEncode(address.toMap()));
                              if (recentSearches.length > 5)
                                recentSearches.removeLast();
                              print('called');
                              shared.setStringList(
                                  'recentSearches', recentSearches);
                              Navigator.of(context).pop(predictions[index]);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Text(
                                    predictions[index].description!,
                                    style: TextStyle(fontSize: 15),
                                    maxLines: 2,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      })
                  : ListView(
                      children: [
                        heightSizedBox(15.0),
                        AddressItem(
                          onTap: () =>
                              navigationPush(context, SelectLocation()),
                          icon: Icons.my_location_rounded,
                          title: 'current location',
                          subTitle: 'Using GPS',
                          color: redColor,
                        ),
                        heightSizedBox(15.0),
                        Divider(thickness: 10.0),
                        heightSizedBox(10.0),
                        Text('Saved Addresses',
                            style: TextStyle(
                                fontSize: 18.0, color: Colors.grey.shade600)),
                        heightSizedBox(20.0),
                        ListView.builder(
                            itemCount: 2,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            padding: EdgeInsets.zero,
                            itemBuilder: (BuildContext context, int index) {
                              return AddressItem(
                                icon: Icons.location_on_outlined,
                                title: 'other',
                                subTitle:
                                    '123,Nice,Dareyaganj,New Delhi, Delhi 10002',
                                color: Colors.black,
                              );
                            }),
                        heightSizedBox(20.0),
                        Text('Recent Searches',
                            style: TextStyle(
                                fontSize: 18.0, color: Colors.grey.shade600)),
                        heightSizedBox(20.0),
                        ListView.builder(
                            itemCount: recentSearches.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            padding: EdgeInsets.zero,
                            itemBuilder: (BuildContext context, int index) {
                              Address recent = Address.fromJSON(
                                  json.decode(recentSearches[index]));
                              return AddressItem(
                                icon: Icons.location_on_outlined,
                                subTitle: recent.area,
                                color: Colors.black,
                              );
                            }),
                      ],
                    ),
            ),
            heightSizedBox(10.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'powered by  ',
                  style: TextStyle(fontSize: 17.0, color: Colors.grey.shade600),
                ),
                Image.asset('assets/icons/google3.png', height: 22)
              ],
            ),
            heightSizedBox(10.0),
          ],
        ),
      ),
    );
  }

  void autoCompleteSearch(String value) async {
    var result = await googlePlace!.autocomplete.get(value);
    print(result!.status);
    if (result.predictions != null && mounted) {
      setState(() {
        predictions = result.predictions!;
      });
    }
  }
}

// sendDataToServer() async {
//   // bool checkNet = await checkInternetConnection();
//   // if (checkNet) {
//   print('connected');
//   List<String> myListOfSurvey2 = [];
//   // SharedPreferences prefs = await SharedPreferences.getInstance();
//   List<String> myListOfSurvey =
//       (prefs.getStringList('mySurveyList') ?? List<String>());
//   print('Your list  $myListOfSurvey');
//   print('Your list length ${myListOfSurvey.length}');
//   if (myListOfSurvey.length > 0) {
//     myListOfSurvey2.addAll(myListOfSurvey);
//     for (int i = 0; i < myListOfSurvey2.length; i++) {
//       submitLocalSurvey(myListOfSurvey2.elementAt(i)).then((value) async {
//         if (value.status == 'success') {
//           myListOfSurvey.remove(myListOfSurvey2.elementAt(i));
//           print(myListOfSurvey.length);
//           print('done');
//           await prefs.setStringList('mySurveyList', myListOfSurvey);
//         }
//       });
//     }
//   }
//   // }
// }
