import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '/Elements/loader.dart';
import '/Model/category.dart';
import '/util/common.dart';
import '/util/style.dart';

class FootItem extends StatelessWidget {
  final Food? food;

  FootItem({this.food});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        width: 120,
        decoration: BoxDecoration(
          color: cartColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.8),
                blurRadius: 2,
                offset: Offset(-0.1, -0.1)),
          ],
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Card(
                margin: EdgeInsets.zero,
                elevation: 3,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    width: double.infinity,
                    imageUrl: food!.url!,
                    placeholder: (context, url) => Loader(height: 100),
                    // placeholder: (context, url) => Image.asset(
                    //     'assets/img/loading.gif',
                    //     fit: BoxFit.cover),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            heightSizedBox(10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    food!.dishName!,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    maxLines: 1,
                    style: TextStyle(fontSize: 15),
                  ),
                  heightSizedBox(7.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('\u20B9${food!.price!}',
                          style: TextStyle(fontSize: 13)),
                      Row(
                        children: [
                          Icon(Icons.star, color: yellowColor, size: 15),
                          Text("3.5", style: TextStyle(fontSize: 13)),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            heightSizedBox(10.0)
          ],
        ),
      ),
    );
  }
}
