class URLConstants {
  static const baseUrl =
      "http://bitesvilla-test.us-east-2.elasticbeanstalk.com/";

  ///API onBoarding************************************************************************
  static const loginApiUrl = baseUrl + 'api/users/login/';
  static const logoutApiUrl = baseUrl + 'api/users/logout/';
  static const signUpApiUrl = baseUrl + 'api/users/createUser/';
  static const isEmailExists = baseUrl + 'api/users/isEmailExists/';
  static const updateGoogleUser = baseUrl + 'api/users/updateGoogleUser/';

  //API ForgotPassword Email
  static const sendOtpEmail = baseUrl + 'api/users/sendOtpToEmail/';
  static const resentOtpEmail = baseUrl + 'api/users/resendOtpToEmail/';
  static const verifyOtpEmail = baseUrl + 'api/users/verifyOtpFromEmail';
  static const resetPassEmail = baseUrl + 'api/users/resetPasswordFromEmail';

  //API ForgotPassword Mobile
  static const sendOtpMbl = baseUrl + 'comm/sendOtp';
  static const resentOtpMbl = baseUrl + 'comm/resendOtp';
  static const verifyOtpMbl = baseUrl + 'comm/verifyOtp';
  static const resetPassFromMbl = baseUrl + 'api/users/resetPasswordFromPhone';

  ///API Home************************************************************************
  static const currentScreen = baseUrl + 'api/users/screen/current';
  static const userHome = baseUrl + 'api/users/userHome';
  static const getSellersDish = baseUrl + 'api/dish/sellerId';

  // ! SELLER REGISTER
  static const sellerReg = baseUrl + 'api/seller/createSellerWithUpload';
  static const selDishAdd = baseUrl + 'api/dish/CreateDish';
}
